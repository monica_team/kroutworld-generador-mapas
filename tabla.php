<?php

$array_mapa = array();


$tierra = 0;
for($x=0;$x<25; $x++) {
	for($y=0;$y<25; $y++) {
		if($x > 3 && $x < 22 && $y > 5 && $y < 20) {
			if(($x >= rand(3,9) && $y >= rand(3,12)) && ($x <= rand(14,22) && $y <= rand(15,22))) {
				$si = rand(0,4);
				if($si == 0) {
					$array_mapa[$x][$y]['tierra'] = 1;
					$array_mapa[$x][$y]['tile'] = "tierra";
				}
				else {
					$array_mapa[$x][$y]['tierra'] = 0;
				}
				
			}
			else {
				$array_mapa[$x][$y]['tierra'] = 1;
				$array_mapa[$x][$y]['tile'] = "tierra";
				$tierra = 1;
			}

			
		} 
		else {
			if($x > 2 && $x < 23 && $y > 4 && $y < 21) {
				$ti = rand(0,2);
				$tierra = 0;
				if($ti == 0) {
					if($array_mapa[$x][$y-1]['tierra'] == 1 || $y >= 3) {
						$array_mapa[$x][$y]['tierra'] = 1;
					}
					else {
						$array_mapa[$x][$y]['tierra'] = 0;
					}
					
				}
				else {						
					$array_mapa[$x][$y]['tierra'] = 0;
				}
			}
			else {
				$array_mapa[$x][$y]['tierra'] = 0;
			}
		}
	}
}




for($x=0;$x<25; $x++) {
	for($y=0;$y<25; $y++) {
		if($array_mapa[$x-1][$y]['tierra'] == 0 && $array_mapa[$x][$y]['tierra'] == 1) {
			$array_mapa[$x-1][$y]['tierra'] = 1;
		}


		if($array_mapa[$x][$y]['tierra'] == 1) {
			if($array_mapa[$x-1][$y]['tierra'] == 1 && $array_mapa[$x+1][$y]['tierra'] == 0 && $array_mapa[$x][$y+1]['tierra'] == 0 && $array_mapa[$x][$y-1]['tierra'] == 1) {
				$array_mapa[$x][$y]['tile'] = "mar_derecha_inferior";
			}
		}
	}
}




echo '<table border="0">';
for($x=0;$x<25; $x++) {
	echo '<tr>';
	for($y=0;$y<25; $y++) {
		if($array_mapa[$x][$y]['tierra'] == 1) {
			$pos_x = $y;
			$pos_y = $x;

			if($array_mapa[$x][$y]['tierra'] == 1 ) {
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mar_derecha";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mar_derecha_inferior";
				}


				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mar_izquierda";
				}


				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_1]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mar_izquierda_inferior";
				}


				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_izquierda";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_derecha";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_superior";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_inferior";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_lateral";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_vertical";
				}



				//-------Mini peninsula---
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra_mar_abajo";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra_mar_arriba";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra_mar_izquierda";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra_mar_derecha";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mini_isla";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra";
				}

				if($array_mapa[$x][$y]['tile'] == NULL) {
					$array_mapa[$x][$y]['tile'] = "mar";
					$array_mapa[$x][$y]['tierra'] = 0;
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mar_derecha";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mar_derecha_inferior";
				}


				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mar_izquierda";
				}


				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_1]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mar_izquierda_inferior";
				}


				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_izquierda";
				}


				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_derecha";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_superior";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mitad_tierra_inferior";
				}


				//-------Mini peninsula---
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra_mar_abajo";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra_mar_arriba";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra_mar_izquierda";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra_mar_derecha";
				}
				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 0 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 0 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 0 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 0) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "mini_isla";
				}

				if($array_mapa[$pos_y][$pos_x+1]['tierra'] == 1 && $array_mapa[$pos_y][$pos_x-1]['tierra'] == 1 && $array_mapa[$pos_y+1][$pos_x]['tierra'] == 1 && $array_mapa[$pos_y-1][$pos_x]['tierra'] == 1) {
					$array_mapa[$pos_y][$pos_x]['tile'] = "tierra";
				}

			}



			echo '<td><img src="tiles/'.$array_mapa[$x][$y]['tile'].'.png" title="X: '.$x.' Y: '.$y.'"></td>';
		}
		else {
			echo '<td><img src="tiles/mar.png"></td>';
		}
	}
	echo '</tr>';
}
echo '</table>';


?>